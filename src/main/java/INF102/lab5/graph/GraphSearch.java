package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Set<V> uNeighbournodes = graph.getNeighbourhood(u);

        if(graph.getNeighbourhood(v).isEmpty() ||graph.getNeighbourhood(u).isEmpty() )
            return false;

        for (V uNeighbournode : uNeighbournodes) {
            if(graph.hasEdge(uNeighbournode, u)){
                return true;
            }
            else return connected(uNeighbournode, u);
        }
        return false;
    }
}
